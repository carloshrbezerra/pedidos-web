import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../global/md/md.module';
import { MaterialModule } from '../app.module';

import { DetalheComponent } from './detalhe.component';
import { DetalheRoutes } from './detalhe.routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DetalheRoutes),
        FormsModule,
        MdModule,
        MaterialModule
    ],
    declarations: [DetalheComponent]
})

export class DetalheModule { }
