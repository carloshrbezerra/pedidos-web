import { Routes } from '@angular/router';
import { AuthRoutes } from '../auth/auth.routing';
import { DetalheComponent } from './detalhe.component';

export const DetalheRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'detalhe/:id',
        component: DetalheComponent,
      },
    ],
  },
];