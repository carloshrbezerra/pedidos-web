import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TableData } from '../global/md/md-table/md-table.component';
import { LegendItem, ChartType } from '../global/md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import { ActivatedRoute } from '@angular/router';
import { Pedido } from '../base/domain/pedido.model';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html'
})
export class DetalheComponent implements OnInit {


  pedido: Pedido;

  listaPedidos = [
    ['Pedido do Carlos', 1.280, 10.00, 'A Vista', 1, 'Rua do oriente 878', '16 de Agosto de 2021', 'Pedido Separado'],
    ['Pedido do Carrefour', 1.580, 20.00, 'A Vista', 2, 'Rua de teste 099', '17 de Agosto de 2020', 'Pedido Recebido'],
  ];



  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.route.params.subscribe(parametros => {

      if (parametros['id']) {
        this.listaPedidos.forEach(e => {

          console.log(e);
          if (e[4] == parametros['id']) {

            this.pedido = new Pedido();

            this.pedido.nome = e[0];
            this.pedido.valorTotal = e[1];
            this.pedido.desconto = e[2];
            this.pedido.formaPagamento = e[3];
            this.pedido.enderecoEntrega = e[5];
            this.pedido.previsaoEntrega = e[6];
            this.pedido.descricaoStatus = e[7];
          }

        });
      }
    });

  }

}
