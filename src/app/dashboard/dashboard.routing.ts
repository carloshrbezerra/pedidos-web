import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthRoutes } from '../auth/auth.routing';

export const DashboardRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
    ],
  },
];
