import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TableData } from '../global/md/md-table/md-table.component';
import { LegendItem, ChartType } from '../global/md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import { Router } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  public listaPedidos: TableData;


  constructor(private router: Router) {

  }

  ngOnInit() {
    this.listaPedidos = {
      headerRow: ['Nome', 'valo Total', 'Total de descontos', 'Forma pagamento'],
      dataRows: [
        ['Pedido do Carlos', '1.280,40', '10,00', 'A Vista', '1'],
        ['Pedido do Carrefour', '1.580,40', '20,00', 'A Vista', '2'],
      ],
    };

  }

  public exibirDetalhes(id: Number) {
    this.router.navigate([`/detalhes/detalhe`, id]);
  }

}
