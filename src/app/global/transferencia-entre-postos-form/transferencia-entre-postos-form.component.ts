import { Component, OnInit, Input, ViewEncapsulation, Inject } from '@angular/core';
import { Transferencia } from 'src/app/base/domain/transferencia.model';
import { TransferenciaDTO } from 'src/app/base/domain/TransferenciaDTO.model';
import { Posto } from 'src/app/base/domain/posto.model';
import { PostoService } from 'src/app/base/services/posto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReturnAPI } from 'src/app/base/domain/return-api.model';

interface DataType {
  transferencia: Transferencia;
}
@Component({
  selector: 'app-transferencia-entre-postos-form',
  templateUrl: './transferencia-entre-postos-form.component.html',
  styleUrls: ['./transferencia-entre-postos-form.component.css'],
})
export class TransferenciaEntrePostosFormComponent implements OnInit {
  transferenciaDTO = new TransferenciaDTO();
  postos: Posto[];
  postoOrigem: Posto;
  postoDestino: Posto;
  transferencia: Transferencia;

  constructor(
    private postoService: PostoService,
    @Inject(MAT_DIALOG_DATA) public data: DataType,
    public dialogRef: MatDialogRef<TransferenciaEntrePostosFormComponent>,
  ) {
    this.postoOrigem = this.data.transferencia.posto;
    this.transferencia = this.data.transferencia;

    this.postoService.listarPostos().subscribe((response: ReturnAPI<Posto[]>) => {
      this.postos = response.object;
    });
  }

  ngOnInit(): void {}

  confirmar(): void {
    this.transferenciaDTO.idPostoOrigem = this.postoOrigem.id;
    this.transferenciaDTO.idTransferencia = this.transferencia.id;
    this.transferenciaDTO.postoOrigemUID = this.postoOrigem.uid;
    this.transferenciaDTO.postoDestinoUID = this.postoDestino.uid;
    this.transferenciaDTO.valor = this.transferencia.valorRealizado;
    this.dialogRef.close(this.transferenciaDTO);
  }

  setPostoDestino(posto: Posto): void {
    this.postoDestino = posto;
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  generateMask(value: number): string {
    return value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
  }
}
