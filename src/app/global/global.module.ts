import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaixaListComponent } from './caixa-list/caixa-list.component';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from './components/components.module';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { CaixaMovimentacoesComponent } from './caixa-movimentacoes/caixa-movimentacoes.component';
import { TransferenciaListComponent } from './transferencia-list/transferencia-list.component';
import { TabViewModule } from 'primeng/tabview';
import { ConfirmacaoDialogComponent } from './confirmacao-dialog/confirmacao-dialog.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { TransferenciaEntrePostosFormComponent } from './transferencia-entre-postos-form/transferencia-entre-postos-form.component';
import { PostoService } from '../base/services/posto.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    CaixaListComponent,
    CaixaMovimentacoesComponent,
    TransferenciaListComponent,
    ConfirmacaoDialogComponent,
    TransferenciaEntrePostosFormComponent,
  ],
  exports: [CaixaListComponent, CaixaMovimentacoesComponent],
  imports: [
    RouterModule,
    CommonModule,
    ComponentsModule,
    TableModule,
    PaginatorModule,
    TabViewModule,
    MatMenuModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  providers: [PostoService],
})
export class GlobalModule {}
