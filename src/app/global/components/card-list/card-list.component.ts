import { Component, OnInit, ContentChild, TemplateRef, Input } from '@angular/core';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {

  @Input() name: string;

  @ContentChild('filter', /* TODO: add static flag */ {}) filter: TemplateRef<any>;
  @ContentChild('table', /* TODO: add static flag */ {}) table: TemplateRef<any>;
  isShowFiltros = false;

  constructor() { }

  ngOnInit() {
  }

}
