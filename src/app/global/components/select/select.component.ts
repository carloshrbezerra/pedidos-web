import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { SelectOption } from './select.option.model';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-select',
    templateUrl: './select.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SelectComponent),
            multi: true
        }
    ]
})
export class SelectComponent implements OnInit, ControlValueAccessor {

    @Input() opcoes: SelectOption[];
    onChange: any;
    value: any;
    descricao: string;
    nome: any;
    @Input() label: string;
    @Input() campoid: string;

    constructor() {
    }

    ngOnInit() {
        console.log(this.opcoes);
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngDoCheck() {
       if (!this.descricao && this.value) {
            if (this.campoid) {
                this.descricao = this.value[this.campoid];
            } else {
                this.descricao = this.value;
            }
        }
    }

    valueChange(event) {
        this.opcoes.forEach(opcao => {
            if (event.target.value === opcao.id) {
                this.value = opcao.valor;
                return;
            }
        });
        this.onChange(this.value);
    }

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void { }

    setDisabledState?(isDisabled: boolean): void {
    }

}
