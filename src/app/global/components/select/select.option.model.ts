export class SelectOption {
    id: string;
    valor: any;
    descricao: string;
    selected: boolean;
    constructor() {
        this.selected = false;
    }
}
