import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ButtonsComponent } from './buttons/buttons.component';
import { ComponentsRoutes } from './components.routing';
import { GridSystemComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PanelsComponent } from './panels/panels.component';
import { TypographyComponent } from './typography/typography.component';
import { SmallBoxComponent } from './small-box/small-box.component';
import { CardBoxComponent } from './card-box/card-box.component';
import { CardBoxToolsComponent } from './card-box/card-box-tools/card-box-tools.component';
import { MessageContainerComponent } from './message/message-container/message-container.component';
import { MessageComponent } from './message/message.component';
import { InputDirective } from './input/input.directive';
import { InputComponent } from './input/input.component';
import { ToolsActionsComponent } from './card-box/tools-actions/tools-actions.component';
import { CardListComponent } from './card-list/card-list.component';
import { CheckboxSnComponent } from './checkbox-sn/checkbox-sn.component';
import { MessageService } from './message/message.service';
import { LoginComponent } from '../../auth/login/login.component';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ComponentsRoutes), FormsModule],
  declarations: [
    ButtonsComponent,
    GridSystemComponent,
    IconsComponent,
    NotificationsComponent,
    PanelsComponent,
    TypographyComponent,
    SmallBoxComponent,
    CardBoxComponent,
    CardBoxToolsComponent,
    MessageContainerComponent,
    MessageComponent,
    InputDirective,
    InputComponent,
    ToolsActionsComponent,
    CardListComponent,
    CheckboxSnComponent,
    LoginComponent,
  ],
  exports: [
    FormsModule,
    SmallBoxComponent,
    CardBoxComponent,
    CardBoxToolsComponent,
    MessageContainerComponent,
    InputComponent,
    InputDirective,
    ToolsActionsComponent,
    CardListComponent,
    CheckboxSnComponent,
    LoginComponent,
  ],
  providers: [MessageService],
})
export class ComponentsModule {}
