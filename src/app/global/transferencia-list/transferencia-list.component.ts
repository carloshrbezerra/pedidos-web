import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { QueryOptions } from '../domain/query.options';
import { Transferencia } from 'src/app/base/domain/transferencia.model';
import { LazyLoadEvent } from 'primeng/api';

export type TransferenciaAcao = Transferencia & { acoes: string[] };
export interface TransferenciaAcaoEvent {
  acao: string;
  transferencia: Transferencia;
}

@Component({
  selector: 'app-transferencia-list',
  templateUrl: './transferencia-list.component.html',
  styleUrls: ['./transferencia-list.component.css'],
})
export class TransferenciaListComponent implements OnInit, OnChanges {
  @Input() caixaId: number;
  @Input() tipoTransferencia: string;
  @Input() rowsNumber: number;
  @Input() totalRecords: number;
  @Input() transferencias: TransferenciaAcao[];

  @Output() transferenciaEvent = new EventEmitter<Object>();
  @Output() loadTransferenciasPage = new EventEmitter<number>();
  @Output() acaoClick = new EventEmitter<TransferenciaAcaoEvent>();

  loading = false;
  queryOptions = new QueryOptions();
  title: string;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['transferencias']) {
      this.loading = false;
    }
  }

  loadTransferencias(event: LazyLoadEvent): void {
    this.loading = true;
    const pageNumber = Math.floor(event.first / event.rows);
    this.loadTransferenciasPage.emit(pageNumber);
  }

  onAcaoClick(acao: string, domain: Transferencia): void {
    this.acaoClick.emit({ acao, transferencia: domain });
  }

  generateMask(value: number): string {
    return value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
  }
}
