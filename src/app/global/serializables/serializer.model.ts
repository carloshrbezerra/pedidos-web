import { Resource } from '../domain/resource.model';

export interface Serializer {
  fromJson(json: any): Resource;
  toJson(resource: Resource): any;
}
