import { Injectable } from '@angular/core';
import { ReturnAPI } from 'src/app/base/domain/return-api.model';
import { MatDialog } from '@angular/material/dialog';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(public _matDialog: MatDialog) {}

  feedbackApi<T>(returnApi: ReturnAPI<T>, mensagemSucesso: string): boolean {
    if (returnApi.success) {
      this.feedbackSuccess(mensagemSucesso);
      return true;
    } else if (returnApi.messages[0].text != null) {
      this.feedbackInfo(`${returnApi.messages[0].text}`);
    } else {
      this.feedbackError('Houve um erro ao executar a operação!');
    }

    return false;
  }

  feedbackSuccess(message: string, afterClosed?: () => void): void {
    swal('Sucesso', message, 'success');
  }

  feedbackInfo(message: string, afterClosed?: () => void): void {
    swal('Info', message, 'info');
  }

  feedbackError(message: string): void {
    swal('Error', message, 'error');
  }
}
