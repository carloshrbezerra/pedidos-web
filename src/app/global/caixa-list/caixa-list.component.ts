import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Caixa } from 'src/app/base/domain/caixa.model';
import { CardListComponent } from '../components/card-list/card-list.component';
import { NgForm } from '@angular/forms';
import { Table } from 'primeng/table';
import { QueryOptions } from '../domain/query.options';
import { CaixaService } from 'src/app/base/services/caixa.service';
import { ReturnAPI, Pageable } from 'src/app/base/domain/return-api.model';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-caixa-list',
  templateUrl: './caixa-list.component.html',
  styleUrls: ['./caixa-list.component.css'],
})
export class CaixaListComponent implements OnInit {
  @ViewChild('searchForm', { static: false }) searchForm: NgForm;
  @ViewChild('dataTable', { static: false }) dataTable: Table;
  @ViewChild(CardListComponent, { static: true }) cardList: CardListComponent;

  @Input() caixas: Caixa[];
  @Input() totalRecords: number;
  @Input() type: string;

  @Output() caixaEvent = new EventEmitter<Caixa>();
  @Output() pageEvent = new EventEmitter<number>();
  @Output() eventList = new EventEmitter();
  @Output() searchEvent = new EventEmitter();

  first = 1;
  queryOptions = new QueryOptions();

  constructor(private caixaService: CaixaService) {}

  ngOnInit(): void {
    this.list();
  }

  eventCaixa(caixa: Caixa): void {
    this.caixaEvent.emit(caixa);
  }

  list(): void {
    this.queryOptions.pageNumber = this.queryOptions.pageNumber === 0 ? 1 : this.queryOptions.pageNumber;
    this.caixaService.pageAll(this.queryOptions, this.type).subscribe((response: ReturnAPI<Pageable<Caixa>>) => {
      this.caixas = response.object.content;
      this.totalRecords = this.caixas.length;
    });
  }

  caixaDetailEvent(caixa: Caixa): void {
    this.caixaEvent.emit(caixa);
  }

  consultar(form: NgForm): void {}

  paginarList(event: LazyLoadEvent): void {
    this.queryOptions.pageNumber = event.first / event.rows;
    this.queryOptions.pageSize = event.rows;

    this.queryOptions.pageNumber = this.queryOptions.pageNumber === 0 ? 1 : this.queryOptions.pageNumber;

    if (this.cardList.isShowFiltros) {
      this.consultar(this.searchForm);
    } else {
      this.list();
    }
  }

  generateMask(value: number): string {
    return value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
  }
}
