import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Caixa } from 'src/app/base/domain/caixa.model';
import { TransferenciaAcaoEvent, TransferenciaAcao } from '../transferencia-list/transferencia-list.component';

@Component({
  selector: 'app-caixa-movimentacoes',
  templateUrl: './caixa-movimentacoes.component.html',
  styleUrls: ['./caixa-movimentacoes.component.css'],
})
export class CaixaMovimentacoesComponent implements OnInit {
  caixaId: number;

  @Input() rowsNumber: number;
  @Input() totalRecords: number;
  @Input() transferencias: TransferenciaAcao[];

  @Output() tabViewChange = new EventEmitter<number>();
  @Output() loadTransferenciasPage = new EventEmitter<number>();
  @Output() acaoClick = new EventEmitter<TransferenciaAcaoEvent>();

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe((caixa: Caixa) => {
      this.caixaId = caixa.id;
    });
  }

  ngOnInit(): void {}

  tabViewDidChanged(event: { index: number }): void {
    this.tabViewChange.emit(event.index);
  }

  onLoadTransferencias(event: number): void {
    this.loadTransferenciasPage.emit(event);
  }

  onAcaoClick(event: TransferenciaAcaoEvent): void {
    this.acaoClick.emit(event);
  }
}
