import { Usuario } from 'src/app/base/domain/usuario.model';


export class CurrentUser {
  constructor(public usuario: Usuario, public token: string) {}
}
