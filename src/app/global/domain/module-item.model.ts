export class ModuleItem {
    id: number;
    link: any[];
    icon: string;
    nome: string;
    descricao: string;

    static getModuleItem(): ModuleItem[] {
       return  [
            {
                id: 1 , link: ['/basico'], icon: 'fa fa-bar-chart', nome: 'Cadastros', descricao: 'Modulos de cadastro'
            }
          ];
    }

}

