import { query } from 'chartist';

export interface QueryBuilder {
  toQueryMap: () => Map<string, string>;
  toQueryString: () => string;
}

export class QueryOptions implements QueryBuilder {
  public pageNumber: number;
  public pageSize: number;
  public search: string;

  constructor(options?: Partial<QueryOptions>) {
    this.pageNumber = options?.pageNumber ?? 0;
    this.pageSize = options?.pageSize ?? 10000;
    this.search = options?.search ?? '';
  }

  toQueryMap(): Map<string, string> {
    const queryMap = new Map<string, string>();
    queryMap.set('page', `${this.pageNumber}`);
    queryMap.set('size', `${this.pageSize}`);
    queryMap.set('', `${this.search}`);

    return queryMap;
  }

  toQueryString(): string {
    let queryString = '';
    this.toQueryMap().forEach((value: string, key: string) => {
      queryString = queryString.concat(key === '' ? `${value}&` : `${key}=${value}&`);
    });

    return queryString.substring(0, queryString.length - 1);
  }
}
