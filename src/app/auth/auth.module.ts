import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { SharedService } from './shared.service';
import { AuthRoutes } from './auth.routing';

@NgModule({
  declarations: [LoginComponent],
  imports: [CommonModule, RouterModule.forChild(AuthRoutes)],
  providers: [SharedService],
  exports: [LoginComponent],
})
export class AuthModule {}
