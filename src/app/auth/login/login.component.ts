import { Component, TestabilityRegistry } from '@angular/core';
import { Usuario } from 'src/app/base/domain/usuario.model';
import { SharedService } from '../shared.service';
import { UserService } from 'src/app/base/services/user.service';
import { Router } from '@angular/router';
import { MessageService } from '../../global/components/message/message.service';
import { CurrentUser } from '../../global/domain/current-user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  usuario = new Usuario();
  shared: SharedService;
  message: string;

  constructor(private userService: UserService, private router: Router, private notificador: MessageService) {
    this.shared = SharedService.getInstance();
  }

  login(): void {


    var usuario = 'teste@teste.com';
    var senha = 'teste';


    if (this.usuario.email == usuario && this.usuario.password == senha)

      var currentUser = new CurrentUser(this.usuario, "token");
    this.shared.logando(currentUser);
    this.router.navigate(['/dashboard']);
  }
}
