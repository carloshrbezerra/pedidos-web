import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PessoaModule } from './pessoa/pessoa.module';
import { EstadoService } from './services/estado.service';
import { PessoaService } from './services/pessoa.service';
import { UserService } from './services/user.service';
import { BaseRoutes } from './base.routing';
import { CaixaControleModule } from './caixa-controle/caixa-controle.module';
import { CaixaService } from './services/caixa.service';
import { TransferenciaService } from './services/transferencia.service';
import { PostoService } from './services/posto.service';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(BaseRoutes), PessoaModule, CaixaControleModule],
  providers: [EstadoService, PessoaService, UserService, CaixaService, TransferenciaService, PostoService],
  exports: [PessoaModule],
})
export class BaseModule {}
