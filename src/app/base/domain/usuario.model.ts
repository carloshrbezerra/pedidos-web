import { Resource } from 'src/app/global/domain/resource.model';


export class Usuario extends Resource {
  public email: string;
  public password: string;
  public profile = 'ROLE_TECNICAN';
  public version = 0;

  constructor() {
    super();
  }
}
