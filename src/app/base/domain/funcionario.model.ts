import { Resource } from 'src/app/global/domain/resource.model';
import { Pessoa } from './pessoa.model';
import { Posto } from './posto.model';

export class Funcionario extends Resource {

    public pessoa: Pessoa;

    public posto: Posto;

    constructor() {
        super();
    }
}
