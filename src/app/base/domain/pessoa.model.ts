import { Resource } from 'src/app/global/domain/resource.model';
import { Usuario } from './usuario.model';


export class Pessoa extends Resource {
  public assignedUser: Usuario;
  public ativo: boolean;
  public dataNscimento: Date;
  public date: string;
  public documento: string;
  public email: string;
  public name: string;
  public rg: string;
  public tipoPessoa: string;
  public user: Usuario;

  constructor() {
    super();
  }
}
