import { Resource } from 'src/app/global/domain/resource.model';


export class Estado extends Resource {
  public ativo: boolean;
  public codigo: number;
  public nome: string;
  public sigla: string;

  constructor() {
    super();
  }
}
