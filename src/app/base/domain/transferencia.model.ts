import { Resource } from 'src/app/global/domain/resource.model';
import { Caixa } from './caixa.model';
import { Posto } from './posto.model';

export class Transferencia extends Resource {
  public caixaDividaFuncionario: Caixa;

  public caixaFuncionario: Caixa;

  public descricao: String;

  public destino: Caixa;

  public origem: Caixa;

  public natureza: String;

  public posto: Posto;

  public saldoSobra: number;

  public situacao: String;

  public tipo: String;

  public type: String;

  public valorPrevisto: number;

  public valorRealizado: number;

  constructor() {
    super();
  }
}

export class TransferenciaOdin {
  id: bigint;
  valor: number;
  caixaOrigemUID: string;
  caixaDestinoUID: string;
  referencia: bigint;
}
