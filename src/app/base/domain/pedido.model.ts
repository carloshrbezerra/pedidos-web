import { Resource } from 'src/app/global/domain/resource.model';

export class Pedido extends Resource {

  public nome: any;
  public valorTotal: any;
  public desconto: any;
  public formaPagamento: any;
  public enderecoEntrega: any;
  public previsaoEntrega: any;
  public status: any; //1 pedido recebido, 2 aprovado, 3 separado 4 transporte, 5 aguardando entrega
  public descricaoStatus: any;


  constructor() {
    super();
  }
}
