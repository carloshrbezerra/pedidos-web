import { Resource } from 'src/app/global/domain/resource.model';
import { Bairro } from './bairro.model';
import { Estado } from './estado.model';

export class Municipio extends Resource {

    public nomeMunicipio: String;

    public listBairros: Bairro[];

    public estado: Estado;

    constructor() {
        super();
    }
}
