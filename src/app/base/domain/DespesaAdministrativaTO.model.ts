export class DespesaAdministrativaTO {
  idPosto: number;
  idTransferencia: number;
  valor: number;
}
