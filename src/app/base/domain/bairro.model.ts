import { Resource } from 'src/app/global/domain/resource.model';

export class Bairro extends Resource {
  public codigo: number;
  public nome: string;

  constructor() {
    super();
  }
}
