import { Component, OnInit } from '@angular/core';
import { Transferencia } from '../../domain/transferencia.model';
import { ActivatedRoute } from '@angular/router';
import { Caixa } from '../../domain/caixa.model';
import { TransferenciaService } from '../../services/transferencia.service';
import { ReturnAPI, Pageable } from '../../domain/return-api.model';
import { QueryOptions } from 'src/app/global/domain/query.options';
import { TransferenciaAcaoEvent, TransferenciaAcao } from 'src/app/global/transferencia-list/transferencia-list.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmacaoDialogComponent } from 'src/app/global/confirmacao-dialog/confirmacao-dialog.component';
import { TransferenciaEntrePostosFormComponent } from 'src/app/global/transferencia-entre-postos-form/transferencia-entre-postos-form.component';
import { TransferenciaDTO } from '../../domain/TransferenciaDTO.model';
import { DialogService } from 'src/app/global/services/dialog.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DespesaAdministrativaTO } from '../../domain/DespesaAdministrativaTO.model';

@Component({
  selector: 'app-caixa-controle-movimentacoes',
  templateUrl: './caixa-controle-movimentacoes.component.html',
  styleUrls: ['./caixa-controle-movimentacoes.component.css'],
})
export class CaixaControleMovimentacoesComponent implements OnInit {
  private static CONFIRMAR_TRANSFERENCIA_ACTION = 'Confirmar Transferência';
  private static REALIZAR_TRANSFERENCIA_ACTION = 'Realizar Complemento de Posto';
  private static REALIZAR_SANGRIA_ACTION = 'Realizar Sangria Odin';

  private queryOptions = new QueryOptions({ pageSize: 10 });

  caixaId: number;
  transferencias: TransferenciaAcao[];
  tipoMovimentacoes: 'entradas' | 'saidas' = 'entradas';
  totalRecords: number;

  despesaAdministrativaTO = new DespesaAdministrativaTO();

  constructor(
    private route: ActivatedRoute,
    private transferenciaService: TransferenciaService,
    public dialog: MatDialog,
    public dialogService: DialogService,
  ) {
    this.route.params.subscribe((caixa: Caixa) => {
      this.caixaId = caixa.id;
    });
  }

  ngOnInit(): void {
    this.listarTransferencias();
  }

  onLoadTransferencias(event: number): void {
    this.listarTransferencias(event);
  }

  mapearAcoesDeTransferencias(transferencias: Transferencia[]): void {
    const mapearAcoes = (transferencia: Transferencia): TransferenciaAcao => {
      const acoes = [];
      if (transferencia.natureza && transferencia.situacao !== 'COMPLEMENTO_ENTRE_POSTOS_TRANSFERIDO') {
        if (transferencia.situacao === 'CONFIRMACAO_PENDENTE') {
          acoes.push(CaixaControleMovimentacoesComponent.CONFIRMAR_TRANSFERENCIA_ACTION);
        }
        if (transferencia.natureza === 'COMPLEMENTO' && transferencia.situacao === 'CONFIRMADA') {
          acoes.push(CaixaControleMovimentacoesComponent.REALIZAR_TRANSFERENCIA_ACTION);
        }
        if (transferencia.natureza === 'SANGRIA' && transferencia.situacao === 'CONFIRMADA') {
          acoes.push(CaixaControleMovimentacoesComponent.REALIZAR_SANGRIA_ACTION);
        }
      }
      return { ...transferencia, acoes };
    };

    this.transferencias = transferencias?.map(mapearAcoes) ?? [];
  }

  listarTransferencias(page: number = 0): void {
    this.queryOptions.pageNumber = page + 1;

    if (this.tipoMovimentacoes === 'entradas') {
      this.transferenciaService
        .pageTransferenciaByDestinoId(this.queryOptions, this.caixaId)
        .subscribe((response: ReturnAPI<Pageable<Transferencia>>) => {
          this.totalRecords = response.object.totalElements;
          this.mapearAcoesDeTransferencias(response.object.content);
        });
    } else if (this.tipoMovimentacoes === 'saidas') {
      this.transferenciaService
        .pageTransferenciaByOrigemId(this.queryOptions, this.caixaId)
        .subscribe((response: ReturnAPI<Pageable<Transferencia>>) => {
          this.totalRecords = response.object.totalElements;
          this.mapearAcoesDeTransferencias(response.object.content);
        });
    }
  }

  confimarTransferencia(transferencia: Transferencia): void {
    const dialogRef = this.dialog.open(ConfirmacaoDialogComponent, {
      width: '300px',
    });

    if (transferencia.natureza === 'SANGRIA') {
      this.confirmarSangriaOdin(dialogRef, transferencia);
    }

    if (transferencia.natureza === 'COMPLEMENTO') {
      this.confirmarComplemento(dialogRef, transferencia);
    }

    if (transferencia.natureza === 'DESPESA_ADMINISTRATIVA') {
      this.confirmarDespesaAdministrativa(dialogRef, transferencia);
    }
  }

  private confirmarComplemento(dialogRef: MatDialogRef<ConfirmacaoDialogComponent>, transferencia: Transferencia): void {
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.transferenciaService.confirmarComplemento(transferencia.id).subscribe(
          (response: ReturnAPI<void>) => {
            this.dialogService.feedbackApi(response, 'Confirmação realizada com sucesso.');
            this.listarTransferencias();
          },
          (e: HttpErrorResponse) => {
            this.dialogService.feedbackError(`Houve um erro ao confirmar Complemento.\n ${e.message}`);
          },
        );
      }
    });
  }

  private confirmarDespesaAdministrativa(dialogRef: MatDialogRef<ConfirmacaoDialogComponent>, transferencia: Transferencia): void {
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.despesaAdministrativaTO.idPosto = transferencia.posto.id;
        this.despesaAdministrativaTO.valor = transferencia.valorRealizado;
        this.despesaAdministrativaTO.idTransferencia = transferencia.id;

        this.transferenciaService.confirmarDespesaAdministrativa(this.despesaAdministrativaTO).subscribe(
          (response: ReturnAPI<void>) => {
            this.dialogService.feedbackApi(response, 'Confirmação realizada com sucesso.');
            this.listarTransferencias();
          },
          (e: HttpErrorResponse) => {
            this.dialogService.feedbackError(`Houve um erro ao confirmar Despesa.\n ${e.message}`);
          },
        );
      }
    });
  }

  private confirmarSangriaOdin(dialogRef: MatDialogRef<ConfirmacaoDialogComponent>, transferencia: Transferencia): void {
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.transferenciaService.confirmarSangriaOdin(transferencia.id).subscribe(
          (response: ReturnAPI<void>) => {
            this.dialogService.feedbackApi(response, 'Confirmação realizada com sucesso.');
            this.listarTransferencias();
          },
          (e: HttpErrorResponse) => {
            this.dialogService.feedbackError(`Houve um erro ao realizar sangria.\n ${e.message}`);
          },
        );
      }
    });
  }

  realizarTransferenciaComplemento(transferencia: Transferencia): void {
    const dialogRef = this.dialog.open(TransferenciaEntrePostosFormComponent, {
      data: { transferencia: transferencia },
      width: '380px',
    });

    dialogRef.afterClosed().subscribe((transferenciaDTO: TransferenciaDTO) => {
      if (transferenciaDTO) {
        this.transferenciaService.realizarTransferenciaComplemento(transferenciaDTO).subscribe(
          (response: ReturnAPI<void>) => {
            this.dialogService.feedbackApi(response, 'Confirmação realizada com sucesso.');
            this.listarTransferencias();
          },
          (e: HttpErrorResponse) => {
            this.dialogService.feedbackError(`Houve um erro ao confirmar Transferência.\n ${e.message}`);
            this.listarTransferencias();
          },
        );
      }
    });
  }

  async realizarSangriaOdin(transferencia: Transferencia): Promise<void> {
    const dialogRef = this.dialog.open(ConfirmacaoDialogComponent, { width: '300px' });

    const result = await dialogRef.afterClosed().toPromise();
    if (result) {
      try {
        const transferenciaDTO = new TransferenciaDTO();

        transferenciaDTO.idPostoOrigem = transferencia.posto.id;
        transferenciaDTO.idTransferencia = transferencia.id;
        transferenciaDTO.valor = transferencia.valorRealizado;

        const _ = await this.transferenciaService.realizarSangriaOdin(transferenciaDTO).toPromise();
        this.dialogService.feedbackSuccess('Sangria para o caixa odin realizada com sucesso');
        this.listarTransferencias();
      } catch (e) {
        console.log(e);
        this.dialogService.feedbackInfo('Houve um erro ao realizar sangria');
      }
    }
  }

  onChangeTabView(id: number): void {
    this.tipoMovimentacoes = id === 0 ? 'entradas' : 'saidas';
    this.listarTransferencias();
  }

  onAcaoClick(event: TransferenciaAcaoEvent): void {
    if (event.acao === CaixaControleMovimentacoesComponent.CONFIRMAR_TRANSFERENCIA_ACTION) {
      this.confimarTransferencia(event.transferencia);
    }

    if (event.acao === CaixaControleMovimentacoesComponent.REALIZAR_TRANSFERENCIA_ACTION) {
      this.realizarTransferenciaComplemento(event.transferencia);
    }

    if (event.acao === CaixaControleMovimentacoesComponent.REALIZAR_SANGRIA_ACTION) {
      this.realizarSangriaOdin(event.transferencia);
    }
  }
}
