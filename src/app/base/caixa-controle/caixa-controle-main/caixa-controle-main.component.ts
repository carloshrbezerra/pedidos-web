import { Component, OnInit } from '@angular/core';
import { Caixa } from '../../domain/caixa.model';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/auth/shared.service';

@Component({
  selector: 'app-caixa-controle-main',
  templateUrl: './caixa-controle-main.component.html',
  styleUrls: ['./caixa-controle-main.component.css'],
})
export class CaixaControleMainComponent implements OnInit {
  constructor(private router: Router, sharedService: SharedService) {}

  ngOnInit(): void {
    console.log();
  }

  caixaDetail(caixa: Caixa): void {
    this.router.navigate(['base/caixa-controle-movimentacoes/' + caixa.id]);
  }
}
