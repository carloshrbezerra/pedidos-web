import { Pessoa } from '../domain/pessoa.model';
import { DomainSerializer } from 'src/app/global/serializables/domain.serializer';


export class PessoaSerializer extends DomainSerializer<Pessoa> {}
