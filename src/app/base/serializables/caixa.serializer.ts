import { DomainSerializer } from 'src/app/global/serializables/domain.serializer';
import { Caixa } from '../domain/caixa.model';

export class CaixaSerializer extends DomainSerializer<Caixa> {}
