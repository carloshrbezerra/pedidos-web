import { Transferencia } from '../domain/transferencia.model';
import { DomainSerializer } from 'src/app/global/serializables/domain.serializer';

export class TransferenciaSerializer extends DomainSerializer<Transferencia> {}
