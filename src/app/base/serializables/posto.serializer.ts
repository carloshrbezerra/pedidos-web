import { DomainSerializer } from 'src/app/global/serializables/domain.serializer';
import { Posto } from '../domain/posto.model';

export class PostoSerializer extends DomainSerializer<Posto> {}
