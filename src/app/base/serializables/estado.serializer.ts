
import { Estado } from '../domain/estado.model';
import { DomainSerializer } from 'src/app/global/serializables/domain.serializer';

export class EstadoSerializer extends DomainSerializer<Estado> {}
