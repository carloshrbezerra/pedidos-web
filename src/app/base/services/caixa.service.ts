import { Injectable } from '@angular/core';
import { Caixa } from '../domain/caixa.model';
import { ResourceService } from 'src/app/global/services/resource.service';
import { WEB_API } from 'src/app/global/services/web-api';
import { CaixaSerializer } from '../serializables/caixa.serializer';
import { HttpClient } from '@angular/common/http';
import { QueryOptions } from 'src/app/global/domain/query.options';
import { Observable } from 'rxjs';
import { ReturnAPI, Pageable } from '../domain/return-api.model';

@Injectable()
export class CaixaService extends ResourceService<Caixa> {
  static RESOURCE = '';

  headers = {
    Authorization:
      'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBnbWFpbC5jb20iLCJjcmVhdGVkIjoxNTk3NzU1NTcwNzUyLCJleHAiOjE1OTgzNjAzNzB9.eUZHYjk-d9bEzHNVHOJL7T0KwP5fPEfX-LCDCChe0eI5GXUBboKCX2zC6m9xtPPVSZduW6SmnWr3JCuMxb_80g',
  };

  constructor(private http: HttpClient) {
    super(http, `${WEB_API}`, 'caixa', new CaixaSerializer(Caixa));
  }

  pageAll(queryOption: QueryOptions, type: string): Observable<ReturnAPI<Pageable<Caixa>>> {
    return this.httpClient.get<ReturnAPI<Pageable<Caixa>>>(`${WEB_API}/caixa?${queryOption.toQueryString()}type=${type}`, {
      headers: this.headers,
    });
  }
}
