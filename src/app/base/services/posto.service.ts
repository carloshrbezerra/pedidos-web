import { Injectable } from '@angular/core';
import { ResourceService } from 'src/app/global/services/resource.service';
import { Posto } from '../domain/posto.model';
import { PostoSerializer } from '../serializables/posto.serializer';
import { HttpClient } from '@angular/common/http';
import { WEB_API } from 'src/app/global/services/web-api';
import { QueryOptions } from 'src/app/global/domain/query.options';
import { Observable } from 'rxjs';
import { Pageable, ReturnAPI } from '../domain/return-api.model';

@Injectable()
export class PostoService extends ResourceService<Posto> {
  static RESOURCE = '';

  constructor(private http: HttpClient) {
    super(http, `${WEB_API}`, 'caixa', new PostoSerializer(Posto));
  }

  listarPostos(): Observable<ReturnAPI<Posto[]>> {
    const headers = {
      Authorization:
        'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBnbWFpbC5jb20iLCJjcmVhdGVkIjoxNTk3NzU1NTcwNzUyLCJleHAiOjE1OTgzNjAzNzB9.eUZHYjk-d9bEzHNVHOJL7T0KwP5fPEfX-LCDCChe0eI5GXUBboKCX2zC6m9xtPPVSZduW6SmnWr3JCuMxb_80g',
    };
    return this.httpClient.get<ReturnAPI<Posto[]>>(`${WEB_API}/posto/listar-postos`, { headers });
  }
}
