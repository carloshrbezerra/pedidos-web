import { Estado } from './../domain/estado.model';
import { EstadoSerializer } from './../serializables/estado.serializer';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WEB_API } from 'src/app/global/services/web-api';
import { ResourceService } from 'src/app/global/services/resource.service';

@Injectable()
export class EstadoService extends ResourceService<Estado> {
  static RESOURCE = '';

  constructor(private http: HttpClient) {
    super(http, `${WEB_API}`, 'estado', new EstadoSerializer(Estado));
  }
}
