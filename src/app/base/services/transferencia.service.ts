import { Injectable } from '@angular/core';
import { ResourceService } from 'src/app/global/services/resource.service';
import { WEB_API } from 'src/app/global/services/web-api';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { QueryOptions } from 'src/app/global/domain/query.options';
import { Observable } from 'rxjs';
import { Transferencia, TransferenciaOdin } from '../domain/transferencia.model';
import { TransferenciaSerializer } from '../serializables/transferencia.serializer';
import { TransferenciaDTO } from '../domain/TransferenciaDTO.model';
import { ReturnAPI, Pageable } from '../domain/return-api.model';
import { DespesaAdministrativaTO } from '../domain/DespesaAdministrativaTO.model';
import { SharedService } from 'src/app/auth/shared.service';

@Injectable()
export class TransferenciaService extends ResourceService<Transferencia> {
  static RESOURCE = '';
  constructor(private http: HttpClient) {
    super(http, `${WEB_API}`, 'transferencia', new TransferenciaSerializer(Transferencia));
  }

  pageTransferenciaByDestinoId(queryOption: QueryOptions, caixaId: number): Observable<ReturnAPI<Pageable<Transferencia>>> {
    return this.httpClient.get<ReturnAPI<Pageable<Transferencia>>>(
      `${WEB_API}/transferencia/pageable?destino=eq:${caixaId}&${queryOption.toQueryString()}&sort=-id`,
    );
  }

  pageTransferenciaByOrigemId(queryOption: QueryOptions, caixaId: number): Observable<ReturnAPI<Pageable<Transferencia>>> {
    return this.httpClient.get<ReturnAPI<Pageable<Transferencia>>>(
      `${WEB_API}/transferencia/pageable?origem=eq:${caixaId}&${queryOption.toQueryString()}&sort=-id`,
    );
  }

  confirmarComplemento(transferenciaId: number): Observable<ReturnAPI<void>> {
    return this.httpClient.put<ReturnAPI<void>>(`${WEB_API}/transferencia-controle/confirmar-complemento/${transferenciaId}`, null);
  }

  confirmarDespesaAdministrativa(despesaAdministrativaTO: DespesaAdministrativaTO): Observable<ReturnAPI<void>> {
    return this.httpClient.put<ReturnAPI<void>>(
      `${WEB_API}/transferencia-controle/confirmar-despesa-administrativa`,
      despesaAdministrativaTO,
    );
  }

  realizarTransferenciaComplemento(transferenciaDTO: TransferenciaDTO): Observable<ReturnAPI<void>> {
    return this.httpClient.put<ReturnAPI<void>>(`${WEB_API}/transferencia-controle/transferir-complemento`, transferenciaDTO);
  }

  confirmarSangriaOdin(transferenciaId: number): Observable<Object> {
    return this.httpClient.put(`${WEB_API}/transferencia-controle/confirmar-sangria-odin/${transferenciaId}`, null);
  }

  realizarSangriaOdin(transferenciaDTO: TransferenciaDTO): Observable<TransferenciaOdin> {
    return this.httpClient.put<TransferenciaOdin>(`${WEB_API}/transferencia-controle/realizar-sangria-odin`, transferenciaDTO);
  }
}
