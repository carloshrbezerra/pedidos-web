import { Routes } from '@angular/router';
import { PessoaListComponent } from './pessoa/pessoa-list/pessoa-list.component';
import { CaixaControleMainComponent } from './caixa-controle/caixa-controle-main/caixa-controle-main.component';
import { CaixaMovimentacoesComponent } from '../global/caixa-movimentacoes/caixa-movimentacoes.component';
import { CaixaControleMovimentacoesComponent } from './caixa-controle/caixa-controle-movimentacoes/caixa-controle-movimentacoes.component';
import { AuthGuard } from '../auth/interceptors/auth.guard';

export const BaseRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'pessoa',
        component: PessoaListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'caixa-controle',
        component: CaixaControleMainComponent,
        children: [],
        canActivate: [AuthGuard],
      },
      {
        path: 'caixa-controle-movimentacoes/:id',
        component: CaixaControleMovimentacoesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'caixa-movimentacoes/:id',
        component: CaixaMovimentacoesComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];
