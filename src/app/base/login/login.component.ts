import { Component } from '@angular/core';
import { Usuario } from 'src/app/base/domain/usuario.model';
import { SharedService } from '../../auth/shared.service';
import { UserService } from 'src/app/base/services/user.service';
import { Router } from '@angular/router';
import { MessageService } from '../../global/components/message/message.service';
import { CurrentUser } from '../../global/domain/current-user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  usuario = new Usuario();
  shared: SharedService;
  message: string;

  constructor(private userService: UserService, private router: Router, private notificador: MessageService) {
    this.shared = SharedService.getInstance();
  }

  login(): void {
    this.message = '';
    this.userService.login(this.usuario).subscribe(
      (userAuthentication: CurrentUser) => {
        this.shared.logando(userAuthentication);
        this.router.navigate(['/dashboard']);
      },
      (err) => {
        this.shared.token = null;
        this.shared.usuario = null;
        this.shared.showTemplate.emit(false);
        this.notificador.messageErro('Email ou Senha Inválido!');
      },
    );
  }
}
