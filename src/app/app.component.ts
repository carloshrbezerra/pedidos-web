import { Component, OnInit } from '@angular/core';
import { SharedService } from './auth/shared.service';

@Component({
  selector: 'app-my-app',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  showTemplate = false;
  public shared: SharedService;

  constructor() {
    this.shared = SharedService.getInstance();
    if (this.shared.isLoggedIn()) {
      this.showTemplate = true;
    }
  }

  ngOnInit() {
    this.shared.showTemplate.subscribe((show) => (this.showTemplate = show));
  }
}
